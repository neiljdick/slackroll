#!/bin/bash

# where to keep working tree
WorkDir=/home/neil/slackroll
if [ ! -d $WorkDir ]; then
    mkdir -p $WorkDir
fi

# this is important! specify x86 for 32 bit kernels
Arch=x86_64

# get the current running kernel version
CurVer=$(uname -r)

Major=$(uname -r | cut -d. -f1)
Minor=$(uname -r | cut -d. -f2)
Incr=$(uname -r | cut -d. -f3)

KernelUrl=http://www.kernel.org/pub/linux/kernel/v$Major.x

# check to see if our working directory exists
if [ ! -d $WorkDir/roll-linux-$CurVer ]; then

    # check if we can copy an existing version
    if [ -d /usr/src/linux-$CurVer ]; then
        cp -R /usr/src/linux-$CurVer $WorkDir/roll-linux-$CurVer
    else
        # have to fetch it
        echo "Fetching Kernel $CurVer"
        wget -nv -P $WorkDir/ $KernelUrl/linux-$CurVer.tar.gz
        tar xvjf $WorkDir/linux-$CurVer.tar.gz
        mv $WorkDir/linux-$CurVer $WorkDir/roll-linux-$CurVer 
        rm linux-$CurVer.tar.gz
    fi
fi

# source should be ready now.
Next=$(expr $Incr + 1)
Patches=0
Running=1
while [ $Running -eq 1 ]
do
    echo "Fetching Patch $Incr->$Next..."
    wget --quiet -P $WorkDir $KernelUrl/incr/patch-$Major.$Minor.$Incr-$Next.gz
    # this is pretty heavy handed, but if wget fails i'm going to assume
    # there are no new patches for us.
    if [ ! $? -eq 0 ]; then
        echo "Patch Not Found"
        Running=0
    else 
        echo "Applying patch.."
        zcat $WorkDir/patch-$Major.$Minor.$Incr-$Next.gz | patch -p1 -d $WorkDir/roll-linux-$Major.$Minor.$Incr -s 
        # bump the directory name
        mv $WorkDir/roll-linux-$Major.$Minor.$Incr $WorkDir/roll-linux-$Major.$Minor.$Next

        Patches=$(expr $Patches + 1)
        Incr=$(expr $Incr + 1)
        Next=$(expr $Next + 1)
    fi

done

# If Patches = 0 at this point we didn't find any upgrades.
if [ ! $Patches -eq 0 ]; then
    # build the kernel :)
    cd $WorkDir/roll-linux-$Major.$Minor.$Incr

    zcat /proc/config.gz > .config
    make oldconfig
    make -j4 bzImage
    make -j4 modules
    mkdir $WorkDir/Modules
    make modules_install INSTALL_MOD_PATH=$WorkDir/Modules

    # try to make a kernel package
    rm -rf $WorkDir/roll-kernel-$Major.$Minor.$Incr
    mkdir -p $WorkDir/roll-kernel-$Major.$Minor.$Incr/boot
    cp arch/$Arch/boot/bzImage $WorkDir/roll-kernel-$Major.$Minor.$Incr/boot/vmlinuz-roll-$Major.$Minor.$Incr
    cp System.map $WorkDir/roll-kernel-$Major.$Minor.$Incr/boot/System.map-$Major.$Minor.$Incr    
    mkdir $WorkDir/roll-kernel-$Major.$Minor.$Incr/install
    cd $WorkDir/roll-kernel-$Major.$Minor.$Incr
    /sbin/makepkg -l y -c n $WorkDir/roll-kernel-$Major.$Minor.$Incr.txz

else
    echo "No Upgrades Found"
fi

